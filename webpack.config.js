/**
 * Assets Config file
 */

const themeName = 'MyBrinde';
const localServer = {
  path: 'mybrinde.local/',
  port: 3000,
};

const path = require('path');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageMinPlugin = require('imagemin-webpack-plugin').default;

const config = {
  entry: {
    app: './src/js/app.js',
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'build/public/'),
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
      },
      {
        test: /\.(png|gif|jpg|jpeg)$/,
        use: [
          {
            loader: 'url-loader',
            options: { name: 'assets/imgs/[name].[hash:6].[ext]', publicPath: '../', limit: 8192 },
          },
        ],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: [
          {
            loader: 'url-loader',
            options: { name: 'assets/fonts/[name].[hash:6].[ext]', publicPath: '../', limit: 8192 },
          },
        ],
      },
    ],
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
      }),
      new OptimizeCssAssetsPlugin({}),
    ],
  },
  plugins: [
    new BrowserSyncPlugin({
      proxy: localServer.path,
      port: localServer.port,
      files: [],
      ghostMode: {
        clicks: false,
        location: false,
        forms: false,
        scroll: false,
      },
      injectChanges: true,
      logFileChanges: true,
      logLevel: 'debug',
      logPrefix: 'wepback',
      notify: true,
      reloadDelay: 0,
    }),
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: [
        path.join(process.cwd(), 'build/public/*'), 
        path.join(process.cwd(), 'build/wp-content/themes/'+ themeName +'/*'),
        "!" + path.join(process.cwd(), 'build/public/uploads')
      ]
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
    }),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', 'theme-wordpress'),
        to: path.resolve(__dirname, 'build', 'wp-content', 'themes', themeName),
        toType: 'dir',
      },
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', 'plugins'),
        to: path.resolve(__dirname, 'build', 'wp-content', 'plugins'),
        toType: 'dir',
      },
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', 'components'),
        to: path.resolve(__dirname, 'build', 'wp-content', 'themes', themeName, 'attic-templates', 'components'),
        toType: 'dir',
        ignore: ['*.js', '*.scss']
      },
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', 'assets'),
        to: path.resolve(__dirname, 'build', 'public'),
        toType: 'dir',
      },
    ]),
    new ImageMinPlugin({ test: /\.(jpg|jpeg|png|gif|svg)$/i }),
  ],
};

module.exports = config;
