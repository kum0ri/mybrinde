<?php
/**
 * @package Attic Meta Shares
 * @version 1.0
 */
/*
Plugin Name:  Attic Meta Shares
Description:  Header meta tags for facebook and twitter shares
Version:      1.0
Author:       Daniel Vaz
*/

if(!class_exists("AtticMetaShares"))
{

  class AtticMetaShares {
    public function __construct() {
      require_once dirname( __FILE__ ) . '/shareCore.php';

      if( function_exists('acf_add_local_field_group') ) {
        $post_types = get_post_types('', 'names' );
        $posts_in = array ( );

        foreach ($post_types as $post_type) {
          $add = array ( array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => $post_type,
                  ));

          array_push($posts_in,$add);
        }

        $args = array(
            'public'   => true,
            '_builtin' => false
        );
        $output = 'names'; // names or objects, note names is the default
        $operator = 'and'; // 'and' or 'or'


        globalConfig($posts_in);

      }

    } // END public function __construct()

    /**
     * Hook into the WordPress activate hook
     */
    public static function activate()
    {
        // Do something
    }

    /**
     * Hook into the WordPress deactivate hook
     */
    public static function deactivate()
    {
        // Do something
    }
  } // END class BuroWordpress
} // END if(!class_exists("BuroWordpress"))

if(class_exists('AtticMetaShares'))
{
    // Installation and uninstallation hooks
    register_activation_hook(__FILE__, array('AtticMetaShares', 'activate'));
    register_deactivation_hook(__FILE__, array('AtticMetaShares', 'deactivate'));

    // instantiate the plugin class
    $plugin = new AtticMetaShares();
} // END if(class_exists('BuroWordpress'))

?>