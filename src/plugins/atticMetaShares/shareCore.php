<?php

//IN HEADER :::

// if(is_404() || is_search()) { $post_id = -1; } else { $post_id = $post->ID; }
// if( class_exists('acf') ) {
//   $social = getShareInfo(
//       $post_id,
//       "Unilabs Blog",
//       site_url('public/imgs/social/social-default.jpg'),
//       "Unilabs Blog"
//     ); 
// }
// else {
//     $social['title'] = '';
//     $social['description'] = '';
//     $social['image'] = '';
// }

//IN HEADER :::

function getShareInfo($id, $home_title, $default_img) {
  global $post;
  
  //default
  $social = [];
  $social["title"] = "";
  $social["image"] = "";
  $social["description"] = "";
  $social["permalink"] = "";

  // instead of permalink:
  if ( is_tax() ) {
    global $wp_query;
    $term = $wp_query->get_queried_object();
    
    if($term->taxonomy == 'media_center_categories') {
      $social["title"] = "Media Center - " . ucwords(strtolower($term->name)) . " | ";
      $social["image"] = site_url() . '/public/imgs/social/social-media-center.jpg';
      $social["description"] = get_field("social_description");
    }
    
    $social["permalink"] = get_term_link( get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
  }
  elseif( is_post_type_archive() ) {
    $social["permalink"] = get_post_type_archive_link( get_query_var('post_type') );
  }
  elseif(is_home()){
    $social["permalink"] = get_bloginfo('url');
  }
  else {
    if($id)
      $social["permalink"] = get_permalink( $id );
    else
      $social["permalink"] = get_bloginfo('url');
  }

  //SOCIAL STUFF
  
  if(!is_home() && !is_archive() && !is_404()){

    if(get_field('social_title')) {
      $social["title"] = get_field('social_title') . " | ";
    }
    else {
      $social["title"] = get_the_title() . " | ";
    }
    
  }
  else if(is_archive() && !is_tax()){
    if(get_post_type() == 'post') {
      $social["title"] = get_field('social_share_category_title', 'options') . " | ";
      $social["image"] = get_field('social_share_category_image', 'options')['url'];
      $social["description"] = get_field('social_share_category_description', 'options');
    }
  }
  else if(is_404())
    $social["title"] = "404 | ";

  if( is_single() || is_page() ):
    if(get_field("social_image"))
      $social["image"] = get_field("social_image");
    $social["description"] = get_field("social_description");
  else :
    /*Else*/
  endif;

  if(is_front_page())
    $social["title"] = "";

  $social["title"] .= $home_title;

  return $social;
}

function globalConfig( $posts_in ) {

  acf_add_local_field_group(array (
    'key' => 'group_5633b6bd2767f',
    'title' => 'Attic - Social Shares',
    'fields' => array (
      array (
        'key' => 'field_56372c575eb47',
        'label' => 'Título',
        'name' => 'social_title',
        'type' => 'text',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
        'readonly' => 0,
        'disabled' => 0,
      ),
      array (
        'key' => 'field_56372c7c5eb48',
        'label' => 'Descrição para as redes sociais',
        'name' => 'social_description',
        'type' => 'textarea',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'maxlength' => '',
        'rows' => '',
        'new_lines' => '',
        'readonly' => 0,
        'disabled' => 0,
      ),
      array (
        'key' => 'field_56372c8f5eb49',
        'label' => 'Imagem para as redes sociais',
        'name' => 'social_image',
        'type' => 'image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'return_format' => 'url',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
      ),
    ),
    'location' => $posts_in,
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
  ));
}

?>