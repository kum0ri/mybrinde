<div class="cart-list-wrapper">
  <div class="cart-list-header row">
    <div class="xxlarge-12 columns">
      <span class="title">Os produtos que adicionou</span>
    </div>
    <div class="xxlarge-4 columns">
      <button class="send-list">Enviar Lista por Email</button>
    </div>
  </div>
  
  <div class="cart-list-content">
    <div class="cart-list-content-header row">
      <div class="xxlarge-3 columns">
        <span>Artigo</span>
      </div>
      <div class="xxlarge-9 columns">
        <span>Descrição</span>
      </div>
      <div class="xxlarge-4 columns">
        <span>Quantidade</span>
      </div>
    </div>

    <article class="cart-list-content-item row">
      <div class="xxlarge-3 columns">
        <img src="/public/imgs/dev/cart-1.png" alt="" />
      </div>
      <div class="xxlarge-9 columns">
        <div class="cart-item-description">
          <div>
            <span class="color"></span>
          </div>
          <div>
            <span class="title">Headphones Bluetooth com Colunas Integradas</span>
            <span class="price">Desde: € 18.11</span>
          </div>
        </div>
      </div>
      <div class="xxlarge-4 columns">
        <span class="quantity">
          <button class="less">-</button>
          <span class="quantity-number">11</span>
          <button class="more">+</button>
        </span>
      </div>
    </article>

    <article class="cart-list-content-item row">
      <div class="xxlarge-3 columns">
        <img src="/public/imgs/dev/cart-1.png" alt="" />
      </div>
      <div class="xxlarge-9 columns">
        <div class="cart-item-description">
          <div>
            <span class="color"></span>
          </div>
          <div>
            <span class="title">Headphones Bluetooth com Colunas Integradas</span>
            <span class="price">Desde: € 18.11</span>
          </div>
        </div>
      </div>
      <div class="xxlarge-4 columns">
        <span class="quantity">
          <button class="less">-</button>
          <span class="quantity-number">11</span>
          <button class="more">+</button>
        </span>
      </div>
    </article>

    <article class="cart-list-content-item row">
      <div class="xxlarge-3 columns">
        <img src="/public/imgs/dev/cart-1.png" alt="" />
      </div>
      <div class="xxlarge-9 columns">
        <div class="cart-item-description">
          <div>
            <span class="color"></span>
          </div>
          <div>
            <span class="title">Headphones Bluetooth com Colunas Integradas</span>
            <span class="price">Desde: € 18.11</span>
          </div>
        </div>
      </div>
      <div class="xxlarge-4 columns">
        <span class="quantity">
          <button class="less">-</button>
          <span class="quantity-number">11</span>
          <button class="more">+</button>
        </span>
      </div>
    </article>
  </div>
</div>