<div class="cart-form-wrapper"> 
  <h1>Finalizar Pedido</h1>
  <form>
    <input type="text" placeholder="Empresa" name="company" />
    <input type="text" placeholder="Nome" name="name" />
    <input type="email" placeholder="Email" name="email" />
    <input type="phone" placeholder="Telefone" name="phone" />
    <span class="warning">Não obrigatório mas pode ser útil caso precisemos de entrar em contacto consigo. Poderá receber SMS a avisar do envio do orçamento.</span>
    <button type="submit">Enviar Pedido de Orçamento</button>
  </form>
</div>