<div class="product-description-wrapper">
  <div class="row">
    <div class="xxlarge-16 columns">
      <button class="reference">
        <span class="label"><span>REF</span></span>
        <span class="number">PG52602<span>
      </button>
    </div>
  </div>

  <div class="row description-content">
    <div class="xxlarge-14 columns">
      <h1>Headphones Bluetooth com Colunas Integradas</h1>
      <p>Headphones dobráveis super confortáveis e almofadados com bluetooth e dupla função: auriculares + colunas. desenho sóbrio em negro, com painel de controlo integrado. Função mãos-livres, rádio fm, entrada para jack de 3.5 mm e ranhura para cartão micro sd até 32gb de capacidade. Auriculares reversíveis com 2 colunas estéreo (2x3w). Headphones recarregáveis por usb - cabo incluído. Distribuídos em atrativa caixa individual.</p>
      <span class="size"><strong>MEDIDAS:</strong> 18X18X8.5 CM</span>
      
    </div>
  </div>

  <div class="row product-caracteristics">
    <div class="xxlarge-12 columns">
      <span class="colors">CORES DISPONÍVEIS:</span>
      <ul class="colors-list">
        <li><span class="color red"></span></li>
        <li><span class="color blue"></span></li>
        <li><span class="color green"></span></li>
      </ul>

      <div class="print">
        <button class="print-button">
          <span class="label">Com impressão?</span>
          <ul>
            <li>Sim</li>
            <li>Não</li>
          </ul>
        </button>
        <button class="colors-number">
          <span class="label">Qual o numero de cores?</span>
          <ul>
            <li>1</li>
            <li>2</li>
          </ul>
        </button>
      </div>

      <div class="quantity-budget">
        <span class="quantity">
          <button class="less">-</button>
          <span class="quantity-number">11</span>
          <button class="more">+</button>
        </span>

        <button class="get-budget">Pedir orçamento</button>
      </div>
    </div>
  </div>

  <div class="row product-price-table">
    <div class="xxlarge-16 columns">
      <div class="table-header">
        <span>QTD</span>
        <span>>1</span>
        <span>>5</span>
        <span>>15</span>
        <span>>25</span>
        <span>>50</span>
        <span>>100</span>
        <span>>250</span>
        <span>>500</span>
      </div>
      <div class="table-content">
        <span>Preço</span>
        <span>€ 29.73</span>
        <span>€ 23.73</span>
        <span>€ 21.73</span>
        <span>€ 19.73</span>
        <span>€ 17.73</span>
        <span>€ 16.73</span>
        <span>€ 14.73</span>
        <span>€ 12.73</span>
      </div>
    </div>

    <div class="xxlarge-16 columns">
      <p class="iva-warning">Acresce IVA à taxa legal em vigor.</p>
      <p class="highlight">Preço com personalização incluída (1 cor).</p>
    </div>
  </div>
</div>