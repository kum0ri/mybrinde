import { TweenMax, Expo, Power4 } from "gsap/TweenMax";
import Draggable from "gsap/Draggable";

export default class BudgetSlider {
  constructor() {
    this.data = {
      sliderWrapper: document.querySelector('.budget-slider-wrapper'),
      slider: document.querySelector('.budget-slider-wrapper .drag-slider-container')
    };

    this.init();
  }

  init() {
    let _self = this;
    Draggable.create(_self.data.slider, {
      type:"x",
      minDuration: 0.3,
      maxDuration: 0.8,
      edgeResistance: .4,
      bounds: _self.data.sliderWrapper,
      throwProps: true,
      onThrowComplete: function() {
       
      },
      onDragStart: function() {
       
      },
      onThrowUpdate: function() {
        
      },
      onDrag: function() {
      }
    });
  }

  updateState(view) {
    let _self = this;

    switch (view.dataset.routerView) {
      case 'home':

      break;
    }
  }

  kill() {

  }
}