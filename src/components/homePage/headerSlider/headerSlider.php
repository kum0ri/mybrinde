<div class="header-slider-wrapper">
  <div class="row expanded">
    <div class="xxlarge-11 collapsed">
      <div class="swiper-container">
        <ul class="swiper-wrapper">
          <li class="swiper-slide block-bg-cover">
            <img class="element-cover" src="/public/imgs/dev/slider-1.png" alt="image 1" />
            <div class="text-wrapper" data-swiper-parallax="-300" data-swiper-parallax-duration="1000">
              <span class="subtitle">Surpreenda</span>
              <h2>Relógio Smart</h2>
              <a href="#" class="btn">Ver Colecção</a>
            </div>
          </li>
          <li class="swiper-slide block-bg-cover">
            <img class="element-cover" src="/public/imgs/dev/slider-2.png" alt="image 2" />
            <div class="text-wrapper" data-swiper-parallax="-300" data-swiper-parallax-duration="1000">
              <span class="subtitle">Surpreenda</span>
              <h2>Relógio Smart</h2>
              <a href="#" class="btn">Ver Colecção</a>
            </div>
          </li>
        </ul>

        <ul class="swiper-pagination"></ul>
      </div>
    </div>
    <div class="xxlarge-5 collapsed">
      <ul class="cta-wrapper">
        <li>
          <a href="#"> 
            <span>Mais Populares</span>
            <img src="/public/imgs/dev/icon-1.svg" alt="" />            
          </a>
        </li>
        <li>
          <a href="#">
            <span>Essenciais de Verão</span>
            <img src="/public/imgs/dev/icon-2.svg" alt="" />
          </a>
        </li>
        <li>
          <a href="#">
            <span>Especial Eco</span>
            <img src="/public/imgs/dev/icon-3.svg" alt="" />
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>