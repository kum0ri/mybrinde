<div class="most-sold-slider-wrapper">
  <div class="row expanded categories-wrapper align-middle">
    <div class="xxlarge-4 columns">
      <h2>Brindes mais vendidos</h2>
    </div>
    <div class="xxlarge-10 columns">
      <ul class="categories-list">
        <li><a class="btn active" href="#">Vestuário e Bonés</a></li>
        <li><a class="btn" href="#">Sacos e Mochilas</a></li>
        <li><a class="btn" href="#">Escritório</a></li>
        <li><a class="btn" href="#">Desporto</a></li>
        <li><a class="btn" href="#">Casa e Vida</a></li>
      </ul>
    </div>
    <div class="xxlarge-2 columns text-right">
      <a class="btn view-all" href="#">Ver todos</a>
    </div>
  </div>
  <div class="row expanded">
    <div class="xxlarge-16 collapsed">
      <div class="drag-slider-container">
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?> 
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?> 
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?> 
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?> 
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?> 
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?> 
      </div>
    </div>
  </div>
</div>