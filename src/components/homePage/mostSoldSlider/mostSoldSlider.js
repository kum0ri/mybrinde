import { TweenMax, Expo, Power4 } from "gsap/TweenMax";
import Draggable from "gsap/Draggable";

export default class MostSoldSlider {
  constructor() {
    this.data = {
      sliderWrapper: document.querySelector('.most-sold-slider-wrapper'),
      slider: document.querySelector('.most-sold-slider-wrapper .drag-slider-container'),
      loopID: -1,
      canLoop: true,
      translateMax: 0
    };

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    this.init();
  }

  init() {
    let _self = this;
    
    _self.data.translateMax = _self.data.slider.offsetWidth*-1;

    Draggable.create(_self.data.slider, {
      type:"x",
      minDuration: 0.3,
      maxDuration: 0.8,
      edgeResistance: .4,
      bounds: _self.data.sliderWrapper,
      throwProps: true,
      onThrowComplete: function() {
       
      },
      onDragStart: function() {
       
      },
      onThrowUpdate: function() {
        
      },
      onDrag: function() {
      }
    });

    _self.loop();

    _self.data.sliderWrapper.addEventListener('mouseenter', _self.onMouseEnter);
    _self.data.sliderWrapper.addEventListener('mouseleave', _self.onMouseLeave);
    
  }

  onMouseEnter() {
    this.data.canLoop = false;
  }
  onMouseLeave() {
    this.data.canLoop = true;
  }

  loop() {
    let _self = this;
    _self.data.loopID = requestAnimationFrame(() => _self.loop());

  }

  updateState(view) {
    let _self = this;

    switch (view.dataset.routerView) {
      case 'home':

      break;
    }
  }

  kill() {

  }
}