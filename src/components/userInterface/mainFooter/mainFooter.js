import Swiper from 'swiper';


export default class MainFooter {
  constructor() {
    this.data = {
      slider: null
    };

    this.init();
  }

  init() {
    let _self = this;

    _self.data.slider = new Swiper('.main-footer-wrapper .swiper-container', {
      speed: 1000,
      pagination: {
        el: '.main-footer-wrapper .swiper-pagination',
        clickable: true
      },
      autoplay: {
        delay: 2000,
        disableOnInteraction: false
      },
      loop: true,
      watchSlidesProgress: true,
      parallax:true
    })
  }

  updateState(view) {
    let _self = this;

    switch (view.dataset.routerView) {
      case 'home':

      break;
    }
  }

  kill() {

  }
}