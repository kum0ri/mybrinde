<div class="main-footer-wrapper">
  <div class="row expanded">
    <div class="xxlarge-11 collapsed">
     <div class="swiper-container">
        <ul class="swiper-wrapper">
          <li class="swiper-slide block-bg-cover">
            <img class="element-cover" src="/public/imgs/dev/slider-1.png" alt="image 1" />
            <div class="text-wrapper" data-swiper-parallax="-300" data-swiper-parallax-duration="1000">
              <span class="subtitle">Surpreenda</span>
              <h2>Relógio Smart</h2>
              <a href="#" class="btn">Ver Colecção</a>
            </div>
          </li>
          <li class="swiper-slide block-bg-cover">
            <img class="element-cover" src="/public/imgs/dev/slider-2.png" alt="image 2" />
            <div class="text-wrapper" data-swiper-parallax="-300" data-swiper-parallax-duration="1000">
              <span class="subtitle">Surpreenda</span>
              <h2>Relógio Smart</h2>
              <a href="#" class="btn">Ver Colecção</a>
            </div>
          </li>
        </ul>

        <ul class="swiper-pagination"></ul>
      </div>
    </div>
    <div class="xxlarge-5 collapsed">
      <div class="newsletter-container">
        <span class="subtitle">MyBrinde</span>
        <h2>Newsletter</h2>
        <p>Subscreva a nossa newsletter para estar a par das mais recentes novidades e promoções exclusivas. Tenha acesso em primeira-mão a todos os conteúdos que são semanalmente disponibilizados através deste meio.</p>
        <form>
          <input type="email" placeholder="O seu email" required />
          <button class="btn" type="submit">Subscrever</button>
        </form>
      </div>
    </div>
  </div>
  <div class="row expanded secondary-container">
    <div class="xxlarge-11 collapsed">
      <div class=" row xxlarge-up-2">
        <div class="column collapsed square-container">
          <img src="/public/imgs/dev/icon-4.svg" alt="" />
          <h3>ORÇAMENTO SEM<br> COMPROMISSO</h3>
          <p>Escolha o brinde ideal para promover a sua marca e peça orçamento online para obter descontos confidenciais. É simples, cómodo e rápido.</p>
        </div>
        <div class="column collapsed square-container">
          <img src="/public/imgs/dev/icon-5.svg" alt="" />
          <h3>MAQUETE<br> DIGITAL</h3>
          <p>Receberá sempre uma maquete digital grátis e sem qualquer compromisso para ver o resultado final da personalização do seu brinde.</p>
        </div>
      </div>
    </div>
    <div class="xxlarge-5 collapsed">
      <div class="last-container square-container">
        <img src="/public/imgs/dev/icon-6.svg" alt="" />
        <h3>TRANSPORTE<br> GRÁTIS</h3>
        <p>Aqui não há complicações! Connosco, o transporte é sempre grátis, seja qual for o valor da sua encomenda (Portugal Continental).</p>
      </div>
    </div>
  </div>
  <div class="row expanded brand-container">
    <div class="xxlarge-5 collapsed">
      <img class="mybrinde-logo" src="/public/imgs/logo.svg" alt="" />
      <p>A <strong>Mybrinde</strong> é Líder em brindes e promocionais para empresas. Temos toda a produção dentro de portas, controlando assim a qualidade eficazmente e garantindo os melhores preços do mercado, bem como prazos de entrega como nenhuma outra empresa.</p>
    </div>
  </div>
  <div class="row expanded copyright-container">
    <div class="xxlarge-16 collapsed">
      <span>© 2019 Copyright Mybrinde . Todos os direitos reservados.</span>
    </div>
  </div>
</div>