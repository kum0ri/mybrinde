<div class="product-thumb-wrapper">
  <div class="image-wrapper">
    <img src="/public/imgs/dev/product-thumb-1.png" alt="product thumb" />
  </div>
  <div class="content-wrapper">
    <h3 class="title">T-shirt Adulto  150</h3>
    <span class="ref-code">CHMC150</span>
    <span class="price">Desde 1,92€</span>
  </div>
</div>