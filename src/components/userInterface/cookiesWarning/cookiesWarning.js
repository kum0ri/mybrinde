import { TweenMax, Expo, Power4 } from "gsap/TweenMax";

class CookiesWarning {
  constructor() {
    this.wrapper = document.querySelector('.cookies-warning-wrapper')
    this.cookieButton = document.querySelector('.cookies-warning-wrapper button')

    this.init();
  }

  init() {
    let _self = this;

    this.cookieButton.addEventListener('click', () => {
      TweenMax.to(_self.wrapper, 1, {
        yPercent: 100,
        ease: Expo.easeOut
      })
    })
  }

  kill() {

  }
}

const cookiesWarning = new CookiesWarning();
export default  cookiesWarning;