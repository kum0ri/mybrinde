<div class="filters">
  <div class="filters-header">
    <h3>Filtros</h3>
  </div>
	<div class="filter-wrapper">		
    <div class="filter-component">
      <div class="filter-title">
        <h3>Auscultadores</h3>
      </div>
      <div class="hidden">
        <input type="radio" name="type" value="Auriculares">Auriculares
        <input type="radio" name="type" value="Auscultadores">Auscultadores
        <input type="radio" name="type" value="Bluetooth">Bluetooth
        <input type="radio" name="type" value="Jack 3.5 mm">Jack 3.5 mm
      </div>
      <ul class="radio">
        <li class="active">
          <span class="outer-circle"><span class="inner-circle"></span></span>Auriculares
        </li>
        <li>
          <span class="outer-circle"><span class="inner-circle"></span></span>Auscultadores
        </li>
        <li>
          <span class="outer-circle"><span class="inner-circle"></span></span>Bluetooth
        </li>
        <li>
          <span class="outer-circle"><span class="inner-circle"></span></span>Jack 3.5 mm
        </li>
      </ul>
    </div>

    <div class="filter-component color">
      <div class="filter-title">
        <h3>Cores</h3>
      </div>
      <div class="hidden">
        <input type="radio" name="color" value="Amarelo">Amarelo
        <input type="radio" name="color" value="Azul">Azul
        <input type="radio" name="color" value="Branco">Branco
        <input type="radio" name="color" value="Laranja">Laranja
      </div>
      <ul class="radio">
        <li class="active">
          <span class="outer-circle"><span class="inner-circle"></span></span>Amarelo
        </li>
        <li>
          <span class="outer-circle"><span class="inner-circle"></span></span>Azul
        </li>
        <li>
          <span class="outer-circle"><span class="inner-circle"></span></span>Branco
        </li>
        <li>
          <span class="outer-circle"><span class="inner-circle"></span></span>Laranja
        </li>
      </ul>
    </div>

    <div class="filter-component price">
      <div class="filter-title">
        <h3>Preço</h3>
      </div>
      <div class="hidden">
        <input type="text" name="min" value="0">
        <input type="text" name="max" value="9999">        
      </div>
      DRAGGER!!! Amanha com um exemplo!
    </div>
	</div>
</div>