<div class="products-grid-wrapper">
  <div class="grid-header">
    <div class="row">
      <div class="xxlarge-8 columns">
        <?php include(get_stylesheet_directory() . '/attic-templates/components/userInterface/breadcrums/breadcrums.php'); ?>
      </div>
      <div class="xxlarge-8 columns text-right">
        <ul class="products-number-wrapper">
          <li>435 Produtos</li>
          <li>Pág. 1 de 12</li>
        </ul>
      </div>
    </div>

    <div class="row category-title-wrapper">
      <div class="xxlarge-8 columns">
        <h1>Tecnologia e Gadgets</h1>
      </div>
      <div class="xxlarge-8 columns text-right">
        <button class="products-order-btn btn">Mais Recentes Primeiro</button>
      </div>
    </div>

    <div class="row">
      <div class="xxlarge-8 columns">
        <ul class="categories-list">
          <li><button class="btn active" href="#">Até 25€</button></li>
          <li><button class="btn" href="#">Preto</button></li>
          <li><button class="btn" href="#">Branco</button></li>
        </ul>
      </div>
      <div class="xxlarge-8 columns text-right">
        <ul class="list-style">
          <li><button class="btn active" href="#">x2</button></li>
          <li><button class="btn" href="#">x3</button></li>
          <li><button class="btn" href="#">x4</button></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="grid-list">
    <div class="row xxlarge-up-3">
      <div class="column product-item">
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
      </div>
      <div class="column product-item">
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
      </div>
      <div class="column product-item">
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
      </div>
      <div class="column product-item">
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
      </div>
      <div class="column product-item">
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
      </div>
      <div class="column product-item">
        <?php include(get_template_directory() . '/attic-templates/components/userInterface/productThumb/productThumb.php'); ?>
      </div>
    </div>
  </div>

  <div class="grid-pagination">
    <ul>
      <li><a class="active" href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">...</a></li>
    </ul>
  </div>
</div>