<!doctype html>
<html class="no-js fixed-all" lang="en">
  <head>
    <?php global $wp; ?>
    <meta charset=utf-8>
    <title>MyBrinde</title>
    <meta name="Description" content="#">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="google-site-verification" content="">
    <meta name="description" content="MyBrinde">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, viewport-fit=cover" >
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="author" content="burocratik.com">
    <link rel="author" href="humans.txt">
    <link rel="canonical" href="<?= home_url( $wp->request ) ?>">

    <!-- Icons -->

    <link rel="apple-touch-icon" sizes="180x180" href="/public/imgs/id/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/public/imgs/id/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/public/imgs/id/favicon-16x16.png">
    <link rel="manifest" href="/public/imgs/id/site.webmanifest">
    <link rel="mask-icon" href="/public/imgs/id/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/public/imgs/id/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/public/css/app.css">
  </head>

<?php

if( class_exists('BuroWordpress') ) {
  $mobile_info = buro_get_mobile();
  $mobile = buro_is_mobile() ? "mobile" : "";
}
else {
  $mobile = '';
  $mobile_info['type'] = '';
}
if( $mobile_info['type'] == 'phone') $phone = 'phone'; else $phone = '';
if( $mobile_info['type'] == 'tablet') $tablet = 'tablet'; else $tablet = '';


?>

<body class="js-byrefresh js-no-ajax <?= $mobile; ?> <?= $phone; ?> <?= $tablet; ?> ">

  <?php #include_once('attic-templates/components/mainHeader/mainHeader.php'); ?>
  <?php #include_once('attic-templates/components/mainMenu/mainMenu.php'); ?>
  
  <?php include_once('attic-templates/components/userInterface/searchBar/searchBar.php'); ?>
  <?php include_once('attic-templates/components/userInterface/mainMenu/mainMenu.php'); ?>
  <?php include_once('attic-templates/components/userInterface/cookiesWarning/cookiesWarning.php'); ?>

  <div class="scroll-content-wrapper" id="container-wrapper" >
    <div class="views-container"  data-router-wrapper>
      


