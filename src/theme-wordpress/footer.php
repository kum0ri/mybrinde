

</div>
<footer class="footer" role="contentinfo"> </footer>

</div><!--scroll-content-wrapper-->

<div id="loading-page" class="js-loading-page" aria-hidden="true"></div>

<!-- outdated browser  -->
<div id="outdated">
  <h6>Your browser is out of date!</h6>
  <p>Update your browser to view this website correctly. <a id="btnUpdateBrowser" href="http://outdatedbrowser.com/">Update my browser now </a></p>
  <p class="last"><a href="#" id="btnCloseUpdateBrowser" title="Close">&times;</a></p>
<!-- end #outdated browser  -->
</div>
<!-- ============= SCRIPTS ============= -->

<script src="/public/js/app.js"></script>

<!-- ANALYTICS -->
<script>
  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  // ga('create', 'UA-XXXXXX-X', 'auto');
  // ga('send', 'pageview');
</script>

<!-- end #ANALYTICS -->

</body>
</html>
