<?php

  add_action('init', 'products', 1);
  function products() {

    register_taxonomy(
      'products_colors',
      'products',
      array(
        'label' => __( 'Product Colors' ),
        'public' => true,
        // 'rewrite'   => array( 'slug' => 'media-center/category', 'with_front' => false ),
        'hierarchical' => false,
        'show_ui' => false,
        'show_tagcloud' => false,
        'show_in_nav_menus' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count'
      )
    );

    register_taxonomy(
      'products_main_category',
      'products',
      array(
        'label' => __( 'Product Category' ),
        'public' => true,
        // 'rewrite'   => array( 'slug' => 'media-center/category', 'with_front' => false ),
        'hierarchical' => false,
        'show_ui' => false,
        'show_tagcloud' => false,
        'show_in_nav_menus' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count'
      )
    );

    register_post_type('products', array(
        'labels' => array(
        'name' => __('Products'),
        'singular_name' => __('Product'),
        'search_items' => __('Search Products'),
        'add_new_item' => __('Add New Product'),
        'edit_item' => __('Edit Product')
      ),
      'public' => true,
      'show_ui' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'products', "with_front" => false),
      'query_var' => true,
      'exclude_from_search' => false,
      'menu_position' => 20,
      'supports' => array('title'),
      'taxonomies' => array()
      )
    );
  }

  // function rewrite_post_link( $post_link, $id = 0 ){
  //   $post = get_post($id);
  //   if ( is_object( $post ) ){

  //     if($post->post_type == 'airplanes') {
  //       $terms = wp_get_object_terms( $post->ID, 'airplanes_models' );
  //       if( $terms )
  //         return str_replace( '%airplanes_models%' , $terms[0]->slug , $post_link );
  //       else
  //         return str_replace( '%airplanes_models%' , '' , $post_link );
  //     }

  //     if($post->post_type == 'media_center') {
  //       $terms = wp_get_object_terms( $post->ID, 'media_center_categories' );
  //       if( $terms )
  //         return str_replace( '%media_category%' , $terms[0]->slug , $post_link );
  //       else
  //         return str_replace( '%media_category%' , '' , $post_link );
  //     }

  //   }
  //   return $post_link;
  // }
  // add_filter( 'post_type_link', 'rewrite_post_link', 1, 3 );

  // function filter_media( $post_type, $which ) {

  //   // Apply this only on a specific post type
  //   if ( 'media_center' === $post_type ) {
  //     // A list of taxonomy slugs to filter by
  //     $taxonomies = array( 'media_center_categories');
  //   }
  //   else if('airplanes' === $post_type) {
  //     // A list of taxonomy slugs to filter by
  //     $taxonomies = array( 'airplanes_models');
  //   }
  //   else {
  //     return;
  //   }
  
  //   foreach ( $taxonomies as $taxonomy_slug ) {
  
  //     // Retrieve taxonomy data
  //     $taxonomy_obj = get_taxonomy( $taxonomy_slug );
  //     $taxonomy_name = $taxonomy_obj->labels->name;
  
  //     // Retrieve taxonomy terms
  //     $terms = get_terms( $taxonomy_slug );
  
  //     // Display filter HTML
  //     echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
  //     echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
  //     foreach ( $terms as $term ) {
  //       printf(
  //         '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
  //         $term->slug,
  //         ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
  //         $term->name,
  //         $term->count
  //       );
  //     }
  //     echo '</select>';
  //   }
  
  // }
  // add_action( 'restrict_manage_posts', 'filter_media' , 10, 2);
?>
