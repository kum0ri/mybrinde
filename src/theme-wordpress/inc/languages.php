<?php

/*Strings Translations*/
/*Post Types*/
add_action( 'init', 'post_types_lang' );
function post_types_lang(){

 $post_types = get_post_types('', 'objects' );
  foreach ( $post_types as $post_type ) {
    pll_register_string( $post_type->labels->name , $post_type->labels->name, 'Custom Post Types Titles' );
  }
}

// pll_register_string('this is test','this is test','Website Strings');
?>
