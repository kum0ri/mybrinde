<?php

$current_page = 'products-page';
$current_body_class = 'products';

?>

<!-- ============= CONTENT ============= -->
<div class="page-main page-current" data-router-view="products">
  <div class="page-toload <?= $current_page ?>" data-bodyClass="<?= $current_body_class ; ?>">
    <header class="page-header">
      
    </header>

    <main class="page-content" role="main">
      <div class="row">
        <div class="xxlarge-4">
                  
          <?php include(dirname(__FILE__) . '/components/productsPage/filtersSidebar/filtersSidebar.php'); ?>

        </div>
        <div class="xxlarge-12 columns">
          <?php include(dirname(__FILE__) . '/components/productsPage/productsGrid/productsGrid.php'); ?>
        </div>
      </div> 
    </main>

    <footer class="page-footer">
      <?php include(dirname(__FILE__) . '/components/userInterface/mainFooter/mainFooter.php'); ?>
    </footer>
  </div>
</div>