<?php

$current_page = 'products-single-page';
$current_body_class = 'products-single';

?>

<!-- ============= CONTENT ============= -->
<div class="page-main page-current" data-router-view="singleProduct">
  <div class="page-toload <?= $current_page ?>" data-bodyClass="<?= $current_body_class ; ?>">
    <header class="page-header">
      <div class="row">
        <div class="xxlarge-8 columns">
          <?php include(get_stylesheet_directory() . '/attic-templates/components/userInterface/breadcrums/breadcrums.php'); ?>
        </div>
      </div>
    </header>

    <main class="page-content" role="main">
      <div class="row">
        <div class="xxlarge-6 columns">
          <?php include(get_stylesheet_directory() . '/attic-templates/components/productPage/imagesGallery/imagesGallery.php'); ?>
        </div>
        <div class="xxlarge-10 columns">
          <?php include(get_stylesheet_directory() . '/attic-templates/components/productPage/productDescription/productDescription.php'); ?>
        </div>
      </div>

      <div class="row expanded">
        <div class="xxlarge-16 columns">
          <?php include(get_stylesheet_directory() . '/attic-templates/components/productPage/relatedProducts/relatedProducts.php'); ?>
        </div>
      </div>
    </main>

    <footer class="page-footer">
      <?php include(dirname(__FILE__) . '/components/userInterface/mainFooter/mainFooter.php'); ?>
    </footer>
  </div>
</div>