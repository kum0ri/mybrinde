<?php

$current_page = 'cart-page';
$current_body_class = 'cart';

?>

<!-- ============= CONTENT ============= -->
<div class="page-main page-current" data-router-view="cart">
  <div class="page-toload <?= $current_page ?>" data-bodyClass="<?= $current_body_class ; ?>">
    <header class="page-header">
      <div class="row">
        <div class="xxlarge-8 columns">
          <?php include(get_stylesheet_directory() . '/attic-templates/components/userInterface/breadcrums/breadcrums.php'); ?>
        </div>
      </div>

      <div class="row">
        <div class="xxlarge-6 columns">
          <?php include(get_stylesheet_directory() . '/attic-templates/components/cartPage/cartForm/cartForm.php'); ?>
        </div>
        <div class="xxlarge-10 columns">
          <?php include(get_stylesheet_directory() . '/attic-templates/components/cartPage/cartList/cartList.php'); ?>
        </div>
      </div>
    </header>

    <main class="page-content" role="main">
     
    </main>

    <footer class="page-footer">
      <?php include(dirname(__FILE__) . '/components/userInterface/mainFooter/mainFooter.php'); ?>
    </footer>
  </div>
</div>