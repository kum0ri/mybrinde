<?php

$current_page = 'home-page';
$current_body_class = 'home';

?>

<!-- ============= CONTENT ============= -->
<div class="page-main page-current" data-router-view="home">
  <div class="page-toload <?= $current_page ?>" data-bodyClass="<?= $current_body_class ; ?>">
    <header class="page-header">
      <?php include(dirname(__FILE__) . '/components/homePage/headerSlider/headerSlider.php'); ?>
    </header>

    <main class="page-content" role="main">
      <?php include(dirname(__FILE__) . '/components/homePage/mostSoldSlider/mostSoldSlider.php'); ?> 
      <?php include(dirname(__FILE__) . '/components/homePage/budgetSlider/budgetSlider.php'); ?> 
    </main>

    <footer class="page-footer">
      <?php include(dirname(__FILE__) . '/components/userInterface/mainFooter/mainFooter.php'); ?>
    </footer>
  </div>
</div>