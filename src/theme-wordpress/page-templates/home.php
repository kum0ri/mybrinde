<?php

/*
Template Name: Home
*/

if( class_exists('acf') ) {
	$controller = get_field("config_current_controller");
}
else {
	$controller = '';
}

get_header();

/*Pages - Controllers*/

include (TEMPLATEPATH . '/attic-templates/home.php');

get_footer();

?>
