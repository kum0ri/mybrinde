<?php

define('ROOTPATH', $_SERVER['DOCUMENT_ROOT']);


#-----------------------------------------------------------------#
# CUSTOM POST TYPES
#-----------------------------------------------------------------#
require_once('inc/cpt.php');

#-----------------------------------------------------------------#
# CUSTOM FORMS
#-----------------------------------------------------------------#
  require_once('inc/ajax_functions.php');

#-----------------------------------------------------------------#
# GENERAL OPTIONS PAGE
#-----------------------------------------------------------------#


if( function_exists('acf_add_options_page') ) {
  // $option_page = acf_add_options_page(array(
  //   'page_title'  => 'Definições Gerais',
  //   'menu_title'  => 'Definições Gerais',
  //   'menu_slug'   => 'general-settings',
  //   'capability'  => 'edit_posts',
  //   'redirect'  => false
  // ));

  //Save ACF Jsons in src or build locations to have everything in sync


  if (strpos($_SERVER['HTTP_HOST'], 'local') !== false) {
    add_filter('acf/settings/save_json', 'my_acf_json_save_point_src');

    function my_acf_json_save_point_src( $path ) {
      return str_replace('/build', '', $_SERVER['DOCUMENT_ROOT']) . '/src/theme-wordpress/acf-json';
    }
  }
  else {
    add_filter('acf/settings/save_json', 'my_acf_json_save_point_prod');
    function my_acf_json_save_point_prod( $path ) {
      return get_stylesheet_directory() . '/acf-json';
    }
  }
}

add_filter('show_admin_bar', '__return_false');

function remove_pages_from_search() {
    global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}
add_action('init', 'remove_pages_from_search');

// add_action('init', 'importPosts');
function importPosts() {
  $json = file_get_contents('http://mybrinde.pt/produtos.php?route=feed/web_api/products&category=12&key=key1');
  $obj = json_decode($json);
  
  foreach($obj->products as $item) {
    $newpost = array(
      'post_title' => $item->name,
      'post_type' => 'products',
      'post_status' => 'publish'
    );
    $newpostid = wp_insert_post($newpost);
  }
}

#-----------------------------------------------------------------#
# CONFIG VARIABLES
#-----------------------------------------------------------------#

add_theme_support( 'post-thumbnails' ); 


function admin_theme_style() {
  wp_enqueue_style('theme-admin-style', get_template_directory_uri() . '/inc/theme-admin.css');
  wp_enqueue_script('theme-admin-script', get_template_directory_uri() . '/inc/theme-admin.js', array('jquery'));
}
add_action('admin_enqueue_scripts', 'admin_theme_style');

function truncate($string, $limit, $break=" ", $pad="...")
{
  // return with no change if string is shorter than $limit
  if(strlen($string) <= $limit) return $string;

  $string = substr($string, 0, $limit);
  if(false !== ($breakpoint = strrpos($string, $break))) {
    $string = substr($string, 0, $breakpoint);
  }

  return $string . $pad;
}

//From : http://scottnelle.com/794/wordpress-and-the-youtube-iframe-api/
function my_youtube_player_iframe_api( $html ) {
	if ( false !== strpos( $html, 'youtube' ) ) {
    $html = str_replace( '<iframe', '<iframe id="youtube-video"', $html );
		$html = str_replace( '?feature=oembed', '?feature=oembed&enablejsapi=1', $html );
	}
	return $html;
}

// disable Gutenberg
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_pages', '__return_false', 10);

add_filter( 'oembed_result', 'my_youtube_player_iframe_api', 10, 1 );
//add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );


//Save post Actions
add_action( 'save_post', 'save_posts_actions', 10, 2 );
function save_posts_actions( $post_ID, $post ) {
  /*Purge cache if exists*/
  if (function_exists('w3tc_pgcache_flush')) { w3tc_pgcache_flush(); }

}

//Numbered pagination from: https://wordpress.stackexchange.com/a/250866

function pagination_bar( $custom_query ) {

    $total_pages = $custom_query->max_num_pages;
    $big = 999999999; // need an unlikely integer

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}
//Add query var to "jobs search filters page"
function add_query_vars($aVars) {
  $aVars[0] = "category-offset";
  return $aVars;
}
add_filter('query_vars', 'add_query_vars');

//Remove TinyMCE styles
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars ) {
  // Uncomment to view format of $toolbars

  //echo '< pre >';
  //print_r($toolbars);
  //echo '< /pre >';
  //die();


  // Add a new toolbar called "Very Simple"
  // - this toolbar has only 1 row of buttons
  // $toolbars['Exposicao' ] = array();
  // $toolbars['Exposicao' ][1] = array( 'formatselect','bullist', 'bold', 'link', 'italic', 'alignleft', 'aligncenter', 'alignright', 'alignjustify', 'superscript');

  $toolbars['single' ] = array();
  $toolbars['single' ][1] = array('formatselect','bullist','numlist' , 'table', 'bold', 'link', 'italic', 'paragraphs', 'superscript');

  $toolbars['h3_paragraphs' ] = array();
  $toolbars['h3_paragraphs' ][1] = array('formatselect');
  
  $toolbars['sources' ] = array();
  $toolbars['sources' ][1] = array('numlist', 'italic', 'link');

  // Edit the "Full" toolbar and remove 'code'
  // - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
  if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false ) {
    unset( $toolbars['Full' ][2][$key] );
  }

  // remove the 'Basic' toolbar completely
  unset( $toolbars['Basic' ] );

  // return $toolbars - IMPORTANT!
  return $toolbars;
}
//Remove TinyMCE styles
add_filter( 'tiny_mce_before_init', 'my_format_TinyMCE' );
function my_format_TinyMCE( $in ) {
  $in['block_formats'] = "Heading 2 =h2; Heading 3 =h3; Paragrafo =p; Lead =h4;";
  return $in;
}

/**
 * Send debug code to the Javascript console
 */
function debug_to_console($data) {
  //   if(is_array($data) || is_object($data))
  // {
    echo("<script>console.log(".json_encode($data).");</script>");
  // } else {
  //   echo("<script>console.log(".$data.");</script>");
  // }
}

?>
