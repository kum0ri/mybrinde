// import SiteState from './utils/siteState';
import { getBrowserInfo } from './buroCore/buroUtils';
import vhCheck from 'vh-check';

export let _globalVariables = {
  currentView: null,
  browserObj: getBrowserInfo(),
  // siteState: new SiteState(),
  vhCheckObj: vhCheck('vh-check'),
  buroScroll: null
};
