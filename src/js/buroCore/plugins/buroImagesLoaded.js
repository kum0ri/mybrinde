const imagesLoaded = require('imagesloaded');

class BuroImagesLoaded {
  constructor(container = null) {
    this.images = document.querySelectorAll('img');
    this.inViewport = [];
    this.config = {
      rootMargin: '0px',
      threshold: 0.01
    }  
    this.container = container;  
    this.observer = null;
  }

  init(callback) {
    // console.log("BuroCore: buroImagesLoaded init")

    let that = this;
    
    if(this.container) {
     
      imagesLoaded(this.container, function(instance) {
        // console.log("BuroCore: buroImagesLoaded waited for " + instance.images.length + " to load...", instance);
        callback();
      });
    }
    else {
     
      if(this.images.length <= 0) callback();
      
      let observer = new IntersectionObserver(onIntersection, this.config);
      this.images.forEach(image => {
        observer.observe(image);
      });

      function onIntersection(entries, observer) { 
        entries.forEach(entry => {
          if (entry.intersectionRatio > 0) {
            observer.unobserve(entry.target);
            that.inViewport.push(entry.target);
          }
        });

        imagesLoaded( that.inViewport, function(instance) {
          callback();
          // console.log("BuroCore: buroImagesLoaded waited for " + instance.images.length + " to load...");
          observer.disconnect();
        });
      }
      
    }
  }
}

export default BuroImagesLoaded;