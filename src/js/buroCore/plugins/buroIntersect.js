import { _globalVariables } from '../../globals';

class BuroIntersect {
  constructor(vars) {
    this.target = vars.target;
    this.targetBoundings = this.target.getBoundingClientRect();
    this.objs = vars.objects;
    this.objsBoundings = [];
    this.overlap = false;
    this.lastScroll = -1;

    this.resize = this.resize.bind(this);
    this.init();
  }

  init() {
    let status = {};

    if(!_globalVariables.buroScroll) {
      status = {
        scrollVal: window.pageYOffset
      }
    }
    else {
      status = _globalVariables.buroScroll.getStatus();
    }

    for (var i = 0, len = this.objs.length; i < len; i++) {
      var el = {};

      el.element = this.objs[i];
      el.boundingTop = this.objs[i].getBoundingClientRect().top + status.scrollVal;
      el.boundingBottom = this.objs[i].getBoundingClientRect().bottom + status.scrollVal;
      el.main = false;
      if (this.objs[i].dataset.intersectmain == "")
        el.main = true;

      this.objsBoundings.push(el);
    }

    window.addEventListener("resize", this.resize);
  }

  update() {
    if(!_globalVariables.buroScroll) {
      var status = {
        scrollVal: window.pageYOffset
      }

      if (this.lastScroll - status.scrollVal < 0) {
        status.direction = 'down';
      } else if (this.lastScroll - status.scrollVal > 0) {
        status.direction = 'up';
      }

      this.lastScroll = status.scrollVal <= 0 ? 0 : status.scrollVal;
    }
    else {
      status = _globalVariables.buroScroll.getStatus();
    }

    this.overlap = false;

    for (var i = 0, len = this.objsBoundings.length; i < len; i++) {
      if (this.target.getBoundingClientRect().bottom + status.scrollVal > this.objsBoundings[i].boundingTop && this.target.getBoundingClientRect().top + status.scrollVal < this.objsBoundings[i].boundingBottom) {
        this.overlap = true;

        if (this.objsBoundings[i].main) {
          this.overlap = false;
          break;
        }
      }
    }

    return this.overlap;
  }

  resize() {
    this.objsBoundings = [];
    this.targetBoundings = this.target.getBoundingClientRect();

    if(!_globalVariables.buroScroll) {
      var status = {
        scrollVal: window.pageYOffset
      }
    }
    else {
      var status = _globalVariables.buroScroll.getStatus();
    }

    for (var i = 0, len = this.objs.length; i < len; i++) {
      var el = {};

      el.element = this.objs[i];
      el.boundings = this.objs[i].getBoundingClientRect();

      el.boundingTop = el.boundings.top + status.scrollVal;
      el.boundingBottom = el.boundings.bottom + status.scrollVal;

      this.objsBoundings.push(el);
    }
  }

  kill() {
    window.removeEventListener("resize", this.resize);
  }
}

export default BuroIntersect;