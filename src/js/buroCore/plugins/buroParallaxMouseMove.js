import { _globalVariables } from '../../globals';

export default function buroParallaxMouseMove(multiplyer) {
  if(!multiplyer) multiplyer = .1;
  
  var elements = document.querySelectorAll('[data-parallaxMove]');
  var parallax_array = [];
  var mouseX = 0;
  var mouseY = 0;
  var mouseXAbs = 0;
  var mouseYAbs = 0;
  var loop_id = -1;
  var height = window.outerHeight;
  var width = window.outerWidth;

  var init = function() {

    elements.forEach(function(item, i){
      var elem = {};

      if(item.dataset.movefriction)
        elem.moveFriction = item.dataset.movefriction;
      else
        elem.moveFriction = 1;

      elem.element = item;
      elem.close = item.querySelectorAll('[data-parallaxMoveClose]');
      elem.away = item.querySelectorAll('[data-parallaxMoveAway]');
      elem.offsetY = item.getBoundingClientRect().top;
      elem.offsetX = item.getBoundingClientRect().left;
      elem.height = item.offsetHeight;
      elem.width = item.offsetWidth;
      elem.inView = true;
      elem.isOver = false;
 
      TweenMax.set([elem.close, elem.away], { transformStyle: "preserve-3d" });

      parallax_array.push(elem);
    });

    document.addEventListener("mousemove", mouseMoveFunc);
    window.addEventListener("resize", resizeFunc);

    loop_id = requestAnimationFrame(() => update());
  }

  var update = function() {
    loop_id = requestAnimationFrame(() => update());
    // updateInView();

    for(var i=0,len=parallax_array.length; i<len; i++) {

      if(mouseXAbs > parallax_array[i].offsetX && mouseXAbs < parallax_array[i].offsetX + parallax_array[i].width) {
        parallax_array[i].isOver = true;
        
        TweenMax.to(parallax_array[i].close, 3, {
          x: -0.3 * mouseX*multiplyer*parallax_array[i].moveFriction,
          y: 0.3 * mouseY*multiplyer*parallax_array[i].moveFriction,
          force3D: true,
          ease: Power2.easeOut
        });
        TweenMax.to(parallax_array[i].away, 3, {
          x: -0.15 * mouseX*multiplyer*parallax_array[i].moveFriction,
          y: 0.15 * mouseY*multiplyer*parallax_array[i].moveFriction,
          force3D: true,
          ease: Power2.easeOut
        });
      }
      else {
        parallax_array[i].isOver = false;

        TweenMax.to(parallax_array[i].close, 2, {
          x: 0,
          y: 0,
          ease: Expo.easeOut
        });
        TweenMax.to(parallax_array[i].away, 2, {
          x: 0,
          y: 0,
          ease: Expo.easeOut
        });
      }
    }
  }

  var updateInView = function() {

    if (!_globalVariables.buroScroll) {
      var status = {
        scrollVal: window.pageYOffset
      }
    } else {
      status = _globalVariables.buroScroll.getStatus();
    }

    for(var i=0,len=parallax_array.length; i<len; i++) {
      if(status.scrollVal + height > parallax_array[i].offsetY && status.scrollVal + height < parallax_array[i].offsetY + parallax_array[i].height + height) {
        parallax_array[i].inView = true;
      }
      else {
        parallax_array[i].inView = false;
      }
    }
  }

  var mouseMoveFunc = function(e) {
    mouseX = e.clientX - width/2;
    mouseY = e.clientY - height/2;
    mouseXAbs = e.pageX;
    mouseYAbs = e.pageY;
  }

  var resizeFunc = function() {
    height = window.outerHeight;
    width = window.outerWidth;
  }

  var kill = function() {
    document.removeEventListener("mousemove", mouseMoveFunc);
    window.removeEventListener("resize", resizeFunc);
    window.cancelAnimationFrame(loop_id);
  }
  return {
    init: init,
    update: update,
    kill: kill
  }
}