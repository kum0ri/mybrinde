import { _globalVariables } from '../../globals'
import { scaleBetween } from '../../buroCore/buroUtils'
import VirtualScroll from "virtual-scroll";
import { TweenMax } from "gsap/TweenMax";
import Draggable from "gsap/Draggable";

export default class BuroScroll {
  constructor(wrapper, target, ease, fullPage) {
    this.containerWrapper = document.querySelector(wrapper),
    this.containerScroll = document.querySelector(target),
    this.scrollHandler = null,
    this.scroll_raf = null,
    this.canScroll = true,
    this.canScrollTo = true,
    this.scrollVal = 0,
    this.isBlocked = false,
    this.isHover = true,
    this.isHoverHandler = false,
    this.isHoverHandlerZone = false,
    this.lastScroll = 0,
    this.prevScroll = 0,
    this.status = { scrollVal:0 },
    this.handlerPosition = 0,
    this.handlerHeight = 0,
    this.scrollLimit = this.containerScroll.offsetHeight - 1,
    this.isFullPage = fullPage,
    this.scrollEase = ease,
    this.handlerDrag = null,
    this.globalViewportH = window.innerHeight,
    this.globalViewportW = window.innerWidth,
    this.listeners = [],
    this.virtualScrollObj = new VirtualScroll();

    this.mouseoverContainerScroll = this.mouseoverContainerScroll.bind(this);
    this.mouseleaveContainerScroll = this.mouseleaveContainerScroll.bind(this);
    this.scrollResize = this.scrollResize.bind(this);
    this.blockBuroScroll = this.blockBuroScroll.bind(this);
    this.unblockBuroScroll = this.unblockBuroScroll.bind(this);
    this.scrolHandlerMouseEnter = this.scrolHandlerMouseEnter.bind(this);
    this.scrolHandlerMouseLeave = this.scrolHandlerMouseLeave.bind(this);
    this.mouseMoveFunc = this.mouseMoveFunc.bind(this);
    this.windowFocus = this.windowFocus.bind(this);
    this.windowBlur = this.windowBlur.bind(this);

    this.init();
  }
      
  
  mouseMoveFunc(){
    if(event.clientX > this.globalViewportW - 20)
      this.isHoverHandlerZone = true;
    else
      this.isHoverHandlerZone = false;
  }

  scrolHandlerMouseEnter() {
    this.isHoverHandler = true;
  } 
  scrolHandlerMouseLeave() {
    this.isHoverHandler = false;
  }

  blockBuroScroll() {
    this.canScroll = false;
    this.isBlocked = true;
  }
  unblockBuroScroll() {
    this.canScroll = true;
    this.isBlocked = false;
  }
  
  mouseoverContainerScroll() {
    this.isHover = true;
  }
  mouseleaveContainerScroll() {
    this.isHover = false;
  }

  windowFocus() {
    this.canScroll = true;
  }

  windowBlur() {
    this.canScroll = false
  }


  scrollResize() {
    this.scrollLimit = this.containerScroll.offsetHeight;
    this.status.scrollLimit = this.scrollLimit;
    
    this.handlerHeight =  window.innerHeight*window.innerHeight/this.scrollLimit; 

    this.scrollHandler.style.height = this.handlerHeight + "px";

    if( this.scrollLimit <= window.innerHeight) {
      TweenMax.set('.scroll-handler-wrapper', {
        display: 'none'
      });
    }
    else {
      TweenMax.set('.scroll-handler-wrapper', {
        display: 'block'
      });
    }
  }


  init() {
    let that = this;

    if(_globalVariables.browserObj.isWindows || _globalVariables.browserObj.isFirefox)
      this.scrollEase = this.scrollEase*2;

      this.scroll_raf = requestAnimationFrame(() => this.scrollLoop());

      this.handlerHeight =  this.globalViewportH*this.globalViewportH/this.scrollLimit;
 
      this.scrollLimit = this.containerScroll.offsetHeight - 1;

      this.status.scrollLimit = this.scrollLimit;

      this.containerWrapper.style.height = '100vh';
      this.containerWrapper.style.overflow = 'hidden';
    
      this.containerWrapper.style.willChange = 'transform';
      this.containerWrapper.style.transform = 'translateZ(0px)';
      this.containerWrapper.style.webkitTransform = 'translateZ(0px)';

    document.querySelector('body').style.overflow = 'hidden';
    document.querySelector('html').style.overflow = 'hidden';
    
    let node = document.createElement("span");
    let node2 = document.createElement("span");

    node.className = 'scroll-handler-wrapper';
    node2.className = 'scroll-handler';

    node.appendChild(node2);
    this.containerWrapper.appendChild(node);
    node.style.position = 'fixed';
    node.style.display = 'block';
    node.style.width = '4px';
    node.style.height = '100%';
    node.style.top = '0px';
    node.style.right = '2px';
    node.style.zIndex = '9999';

    node2.style.position = 'absolute';
    node2.style.display = 'block';
    node2.style.width = '4px';
    node2.style.height = this.handlerHeight + 'px';
    node2.style.top = '0px';
    node2.style.left = '0px';

    if(this.isFullPage) {
      document.querySelector('body').style.overflow = 'hidden';
      document.querySelector('html').style.overflow = '100vh';
    }

    if( this.scrollLimit <= window.innerHeight) {
      TweenMax.set('.scroll-handler-wrapper', {
        display: 'none'
      });
    }
      
    this.scrollHandler = document.querySelector('.scroll-handler');

    this.virtualScrollObj.on(function(e) {
      if(!that.canScroll) return;
      
      that.scrollVal += e.deltaY*that.scrollEase;
      that.scrollVal = Math.max( (that.scrollLimit - that.globalViewportH) * -1, that.scrollVal);
      that.scrollVal = Math.min(0, that.scrollVal);
    });
    
    this.handlerDrag = Draggable.create(".scroll-handler", {
      type: "y",
      bounds: ".scroll-handler-wrapper",
      cursor: 'grab',
      onDrag : function() {
        that.scrollVal = this.endY*(that.scrollLimit)/that.globalViewportH*-1;

        TweenMax.to(that.containerScroll, that.scrollEase,{
          y: that.scrollVal,
          roundProps: "x",
          force3D: true,
          ease: Linear.easeNone
        });  
      },
      onPress : function() {
        this.isDragPressed = true;
        TweenMax.set(".scroll-handler",{cursor:"grabbing"});
      },
      onRelease : function() {
        this.isDragPressed = false;
      }
    });
    
    /*Events*/
    this.containerScroll.addEventListener("mouseover", this.mouseoverContainerScroll);
    this.containerScroll.addEventListener("mouseleave", this.mouseleaveContainerScroll);
    window.addEventListener("resize", this.scrollResize);
    document.addEventListener("blockBuroScroll", this.blockBuroScroll);
    document.addEventListener("unblockBuroScroll", this.unblockBuroScroll);
    this.scrollHandler.addEventListener('mouseenter', this.scrolHandlerMouseEnter);
    this.scrollHandler.addEventListener('mouseleave', this.scrolHandlerMouseLeave);
    document.addEventListener("mousemove", this.mouseMoveFunc);
    window.addEventListener("focus", this.windowFocus);
    window.addEventListener("blur", this.windowBlur);
  }

  update() {
    this.scrollResize();
  }

  setPosition(x,y) {
    TweenMax.set(this.containerScroll,{
      y: y,
      x: x
    })
  }

  scrollTo(val, speed, callback) {
    let that = this;
    that.canScroll = false;
    if(!that.canScrollTo) return;
    that.canScrollTo = false;
    
    let counter = {var: that.scrollVal };
    let value = val;

    TweenMax.to(counter, speed, {
      var: value,
      roundProps: "var",
      ease: Power4.easeInOut,
      onUpdate: function () {
        that.scrollVal = counter.var;
      },
      onComplete: function() {
        if(!that.isBlocked)  that.canScroll = true;
        that.canScrollTo = true;
        if(typeof callback === 'function') callback();
      }
    })
  }

  kill() {
    cancelAnimationFrame(this.scroll_raf);
    this.virtualScrollObj.off();
    this.containerScroll.removeEventListener("mouseover", this.mouseoverContainerScroll);
    this.containerScroll.removeEventListener("mouseleave",this. mouseleaveContainerScroll);
    window.removeEventListener("resize", this.scrollResize);
    document.removeEventListener("blockBuroScroll", this.blockBuroScroll);
    document.removeEventListener("unblockBuroScroll", this.unblockBuroScroll);
    this.scrollHandler.removeEventListener('mouseenter', this.scrolHandlerMouseEnter);
    this.scrollHandler.removeEventListener('mouseleave', this.scrolHandlerMouseLeave);
    document.removeEventListener("mousemove", this.mouseMoveFunc);
    window.removeEventListener("focus", this.windowFocus);
    window.removeEventListener("blur", this.windowBlur);

    document.querySelector(".scroll-handler").remove();
    document.querySelector(".scroll-handler-wrapper").remove();
  }

  addListener(listenerFunction) {
    if (typeof listenerFunction == "function") {
      this.listeners.push(listenerFunction);
    }
  }

  removeListener(listenerFunction) {
    if (typeof listenerFunction == "function") {
      this.listeners.push(listenerFunction);

      for(let i=0,len=this.listeners.length; i<len; i++) {
        if(this.listeners[i] == listenerFunction) {
          this.listeners.splice(i, 1);
        }
      }
    }
  }

  getStatus() {
    return this.status;
  }

  getScrollBarInfo() {
    return {
      position : -this.handlerPosition,
      height : this.handlerHeight
    }
  }
  
  setBarColor(color) {
    TweenMax.to(".scroll-handler", .5, {
      backgroundColor: color
    });
  }

  //private funcions
 scrollLoop(callback) {
   let that = this;
   
  that.scroll_raf = requestAnimationFrame(() => that.scrollLoop());

    if(that.isHoverHandlerZone && !that.isHoverHandler) {
      TweenMax.to(that.scrollHandler, .5, {
        opacity: .5
      })
      document.body.style.cursor = "auto";
    }
    else if(that.isHoverHandler) {
      TweenMax.to(that.scrollHandler, .5, {
        opacity: 1
      })
      document.body.style.cursor = "grab";
    }
    else if(that.prevScroll != that.scrollVal) {
     
      that.handlerPosition = scaleBetween(that.scrollVal, 0, window.innerHeight, 0, that.scrollLimit);
      TweenMax.to(that.scrollHandler, that.scrollEase, {
        y: -that.handlerPosition
      })
      TweenMax.to(that.containerScroll, that.scrollEase,{
        y: that.scrollVal,
        roundProps: "x",
        force3D: true,
        ease: Power2.easeOut
      })

      TweenMax.to(that.scrollHandler, .5, {
        opacity: .5
      })

      //Run listeners
      that.prevScroll > that.scrollVal ? that.status.direction = 'down' : that.status.direction = 'up';
      that.status.scrollVal = -Math.round(that.scrollVal);

      for(let i=0,len=that.listeners.length; i<len; i++) {
        that.listeners[i](status);
      }   

      that.prevScroll = that.scrollVal;
    }
    else {
      TweenMax.to(that.scrollHandler, .5, {
        opacity: 0,
      })
    }
  }
}