import { _globalVariables }  from '../globals.js';

class BuroClass {
  constructor(name) {
    this.name = name;
  }

  init() {
    let event = new CustomEvent('newPageLoaded');
    
    window.dispatchEvent(event);
    // _globalVariables.siteState.updateState();
  }


  animateIn() {
    // console.log("BuroCore: " + this.name + " animateIn")
  }

  animateOut() {
    // console.log("BuroCore: " + this.name + " animateOut")
  }

  kill() {
    let event = new CustomEvent('currentPageKilled');
    window.dispatchEvent(event);
    // console.log("BuroCore: " + this.name + " kill")
  }
}

export default BuroClass;
