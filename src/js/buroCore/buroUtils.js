/** =Manage for added body and page classes */
export const manageBodyClasses = function (newContainer, oldContainer = null) {
  if (oldContainer) {
    document.querySelector('body').classList.remove(oldContainer.getAttribute('data-bodyClass'));
    document.querySelector('body').classList.remove('js-byrefresh');
    document.querySelector('body').classList.remove('js-no-ajax');
  }

  document.querySelector('body').classList.add(newContainer.getAttribute('data-bodyClass'))
}

//Get CSS properties in JS
export const getStyle = function (el, ruleName) {
  return getComputedStyle(el)[ruleName];
}

export const getBrowserInfo = function () {
  var obj = {
    isMobile: false,
    isTablet: false,
    isPhone: false,
    isDesktop: false,
    isPortrait: false,
    isLandscape: false,
    isSafari: false,
    isIE: false,
    isEdge: false,
    isChrome: false,
    isFirefox: false,
    isRetina: false,
    pixelRatio: 1,
    isWindows: false,
    type: '',
    browser: ''
  }

  if (document.querySelector('body').classList.contains("mobile")) {
    obj.isMobile = true;

    if (document.querySelector('body').classList.contains("phone")) {
      obj.isPhone = true;
      obj.type = 'phone';
    } else {
      obj.type = 'tablet';
      obj.isTablet = true;
    }

    if (document.querySelector('html').classList.contains("firefox")) {
      obj.browser = 'firefox';
      obj.isFirefox = true;
    } else if (document.querySelector('html').classList.contains("chrome")) {
      obj.browser = 'chrome';
      obj.isChrome = true;
    } else if (document.querySelector('html').classList.contains("safari")) {
      obj.browser = 'safari';
      obj.isSafari = true;
    } else
      obj.browser = 'unknown';
  } else {
    obj.type = 'desktop';
    obj.isDesktop = true;

    if (document.querySelector('html').classList.contains("win")) {
      obj.isWindows = true;
    }

    if (document.querySelector('html').classList.contains("firefox")) {
      obj.browser = 'firefox';
      obj.isFirefox = true;
    } else if (document.querySelector('html').classList.contains("chrome")) {
      obj.browser = 'chrome';
      obj.isChrome = true;
    } else if (document.querySelector('html').classList.contains("safari")) {
      obj.browser = 'safari';
      obj.isSafari = true;
    } else if (document.querySelector('html').classList.contains("ie")) {
      obj.browser = 'ie';
      obj.isIE = true;
    } else if (document.querySelector('html').classList.contains("edge")) {
      obj.browser = 'edge';
      obj.isEdge = true;
    } else
      obj.browser = 'unknown';
  }

  if (document.querySelector('html').classList.contains("orientation_landscape"))
    obj.isLandscape = true;

  if (document.querySelector('html').classList.contains("orientation_portrait"))
    obj.isPortrait = true;

  obj.pixelRatio = window.devicePixelRatio;

  if (obj.pixelRatio >= 2)
    obj.isRetina = true;

  return obj;
}

export const scaleBetween = function (unscaledNum, minAllowed, maxAllowed, min, max) {
  return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
}