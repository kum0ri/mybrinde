import {TweenMax} from "gsap/TweenMax";
import {  _globalVariables }  from '../globals.js';
import { manageBodyClasses }  from '../buroCore/buroUtils';
import BuroImagesLoaded from '../buroCore/plugins/buroImagesLoaded'
import Highway from '@dogstudio/highway';

// Fade
class Fade extends Highway.Transition {
  out({ from, done }) {
    done();
  }

  in({ from, to, done }) {
    manageBodyClasses(to.querySelector('body .page-toload'), from.querySelector('body .page-toload'));

    let buroImagesLoadedObj = new BuroImagesLoaded(to);

    _globalVariables.morphRightButton.updateState(to);
    _globalVariables.morphLeftButton.updateState(to);
    _globalVariables.startButton.updateState(to, Number(to.querySelector('.page-toload').dataset.activesection));
    _globalVariables.mainMenuObj.updateState(to);

    _globalVariables.currentView.exitAnimation().then(function() {
      buroImagesLoadedObj.init( () => {
        TweenMax.fromTo(from, .25, { 
          opacity: 1,
        },{
          opacity: 0,
          onComplete: function() {
            from.remove();
            TweenMax.fromTo(to, .25,{ 
              opacity: 0 
            },{
              opacity: 1,
              onComplete: function() {
                done();
              }
            });
          }
        });
      });
    });

   
    
  }
}

export default Fade;

