import { _globalVariables }  from '../globals.js';
import Highway from '@dogstudio/highway';
import BuroPage  from '../buroCore/buroPage';
import HeaderSlider from '../../components/homePage/headerSlider/headerSlider'
import MostSoldSlider from '../../components/homePage/mostSoldSlider/mostSoldSlider'
import BudgetSlider from '../../components/homePage/budgetSlider/budgetSlider'
import MainFooter from '../../components/userInterface/MainFooter/MainFooter'

class HomePage extends BuroPage{
  constructor() {
    super('home');
    this.data = {
      headerSlider: new HeaderSlider(),
      mostSoldSlider: new MostSoldSlider(),
      budgetSlider: new BudgetSlider(),
      mainFooter: new MainFooter()
    };
    this.init();
  }

  init() {
    let that = this;

    super.init();
  }
  

  kill() {
    super.kill();
  }
}


class HomeView extends Highway.Renderer {
  // Hooks/methods
  onEnter() { 

  }
  onLeave() {
    _globalVariables.currentView.kill();
  }
  onEnterCompleted() {
    _globalVariables.currentView = new HomePage();
  }
  onLeaveCompleted() {
    
  }
}
export default HomeView;
