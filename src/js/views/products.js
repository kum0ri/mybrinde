import { _globalVariables }  from '../globals.js';
import Highway from '@dogstudio/highway';
import BuroPage  from '../buroCore/buroPage';
import MainFooter from '../../components/userInterface/MainFooter/MainFooter'

class ProductsPage extends BuroPage{
  constructor() {
    super('products');
    this.data = {      
      mainFooter: new MainFooter()
    };
    this.init();
  }

  init() {
    let that = this;

    super.init();
  }
  

  kill() {
    super.kill();
  }
}


class ProductsView extends Highway.Renderer {
  // Hooks/methods
  onEnter() { 

  }
  onLeave() {
    _globalVariables.currentView.kill();
  }
  onEnterCompleted() {
    _globalVariables.currentView = new ProductsPage();
  }
  onLeaveCompleted() {
    
  }
}
export default ProductsView;
