import { _globalVariables }  from '../globals.js';
import Highway from '@dogstudio/highway';
import BuroPage  from '../buroCore/buroPage';
import RelatedProductsSlider from '../../components/productPage/relatedProducts/relatedProducts'

class SingleProduct extends BuroPage{
  constructor() {
    super('single-product');
    this.data = {
      relatedProductsSlider: new RelatedProductsSlider(),
    };
    this.init();
  }

  init() {
    let that = this;

    super.init();
  }
  

  kill() {
    super.kill();
  }
}


class SingleProductView extends Highway.Renderer {
  // Hooks/methods
  onEnter() { 

  }
  onLeave() {
    _globalVariables.currentView.kill();
  }
  onEnterCompleted() {
    _globalVariables.currentView = new SingleProduct();
  }
  onLeaveCompleted() {
    
  }
}
export default SingleProductView;
