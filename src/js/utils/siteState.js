const axios = require('axios');

class SiteState {
  constructor() {
    this.data = {
      activeMainLevel: null,
      activeLevel: null,
      activeView: null,
      activeSection: null,
      websiteData: null
    }
    this.init();
  }

  init(callback) {
    let that = this;
  }

  updateState(nextLevel = null) {
    if(!nextLevel) {
      this.data.activeMainLevel = document.querySelector('.page-toload').dataset.activeMainLevel;
      this.data.activeLevel = document.querySelector('.page-toload').dataset.activeLevel;
      this.data.activeView = document.querySelector('.page-current').dataset.routerView;
    }
    else{
      this.data.activeMainLevel = nextLevel;
      if(document.querySelector('.page-toload').dataset.activesection)
        this.data.activeSection = document.querySelector('.page-toload').dataset.activesection;
    }  
  }
  
  getData() {
    let that = this;
    return new Promise(function(resolve, reject) {
      if(!that.data.websiteData) {
        axios.get('/core/data.json',)
        .then(function (response) {
          that.data.websiteData = response.data;
          resolve(that.data.websiteData);
        })
        .catch(function (error) {
          reject(error);
        });
      }
      else {
        resolve(that.data.websiteData);
      }
    });
  }

  findData(mainLevel = null, subLevel = null, callback = null) { 
    let that = this;
    let promise = that.getData();

    return new Promise(function(resolve, reject) {
      promise.then(function() {
        //Promise resolveu então podemos procurar a info da "BD"
        
        if(mainLevel === null) {
          resolve(that.data.websiteData);
        }
        if(mainLevel === 'violentometro') {
          resolve(that.data.websiteData.home);
        }

        that.data.websiteData.mainLevels.forEach(function(item) {
          
          if(mainLevel == item.slug) {

            if(subLevel == null) {
              resolve(item);
            }
            else {
              item.levels.forEach(function(level) {
                if(subLevel === level.slug)
                resolve(level);
              }) 
            }
          }
        })
        resolve('no data found');
      })
      .catch("API error!") 
    });
  }
  
  findDataByID(id) {
    let that = this;
    let promise = that.getData();

    return new Promise(function(resolve, reject) {
      promise.then(function() {
        //Promise resolveu então podemos procurar a info da "BD"

        that.data.websiteData.mainLevels.forEach(function(item) { 
          item.levels.forEach(function(level) {
            if(id === level.id)
            resolve(level);
          }) 
        })
        resolve('no data found');
      })
      .catch("API error!") 
    });
  }

  kill() {
    // console.log("Violentometro: siteState kill")

  }
}

export default SiteState;
