//CSS
import CssBrowserSelector from 'css-browser-selector';
import '../sass/app.scss';

//Globals
import { _globalVariables }  from './globals.js';
import cookiesWarning from '../components/userInterface/cookiesWarning/cookiesWarning'

//Plugins
import { TweenMax } from "gsap/TweenMax";
import Quicklink from 'quicklink/dist/quicklink.mjs';
import Highway from '@dogstudio/highway';

//BuroPlugins
import { manageBodyClasses }  from './buroCore/buroUtils';
import BuroImagesLoaded from './buroCore/plugins/buroImagesLoaded'

//Transitions
import Fade from './transitions/defaultTransition'

//Views
import HomeView from './views/home';
import ProductsView from './views/products';
import SingleProductView from './views/singleProduct';

/****** First Load *****/ 
// - Wait for above the fold images to load
let buroImagesLoadedObj = new BuroImagesLoaded();
buroImagesLoadedObj.init( () => {
  TweenMax.to('#loading-page', .5, {
    autoAlpha: 0,
    onComplete: function() {
      document.querySelector('#loading-page').remove();
    }
  })
});

// - Add Control Class to body
manageBodyClasses(document.querySelector('body .page-toload'));
/****** First Load *****/ 

/****** Highway *****/ 
const H = new Highway.Core({
  renderers: {
    home: HomeView,
    products: ProductsView,
    singleProduct: SingleProductView
  },
  transitions: {
    default: Fade
  }
});

Quicklink({
  priority: true
});
// Listen Events
H.on('NAVIGATE_END', ({ to, trigger, location }) => {
  Quicklink({
    priority: true
  });
});

window.addEventListener('DOMContentLoaded', function() {
  //DOM LOADED
})
/****** Highway *****/ 

